<?php
	require_once('config.php');
?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<title> Task Manager </title>
		<link rel="stylesheet" type="text/css" href="css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
</head>

<body>
				<?php require_once 'headertitle.php';?>
  <div id="signup">

    <form  class="form" action="Adduser-action.php" method="post">

    <div class="top-row">

      <div class="field-wrap">
        <label>
          Name<span class="req">*</span>
        </label>
        <input type="text"required autocomplete="off" name="name"/>
      </div>
    </div>

    <div class="field-wrap">
      <label>
      Adresse Email<span class="req">*</span>
      </label>
      <input type="email"required autocomplete="off" name="email"/>
    </div>

    <div class="field-wrap">
      <label>
      Mot de passe<span class="req">*</span>
      </label>
      <input type="password"required autocomplete="off" name="password"/>
    </div>

      <button type="submit" class="button button-block"/>S'inscrire</button>
			<input class="button button-block" type="reset" value="Del">
    </form>
  </div>

	<span class="form-send">
	<a href="index.php" class="form-send-button"><button type="button" class="footer-button"><div class="footer-button-label">Back</div></button></a>

	<script src="bower_components/jquery/dist/jquery.js"></script>
	<script src="bower_components/what-input/dist/what-input.js"></script>
	<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
	<script src="js/app.js"></script>
</body>
</html>

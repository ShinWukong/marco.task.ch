<?php
 require_once('config.php');
 require_once('security.php');
?>
<!DOCTYPE html>
<html lang="fr-CH">
   <head>
      <title>Mon Curriculum vitae</title>
      <meta charset="UTF-8">
      <link rel="stylesheet" href="css/app.css">
      <style>

      </style>
   </head>

   <body id="cv">
     <div id="Title">
   			<h1 id="name">Kevin da Silva Marco </h1>
    </div>

   			<br>
   			<img id="image" src="assets/img/2B.jpg" alt="portrait 2B" title="Nier Automata main protagonist">

    <div id="Presentation">
            <ul id="list">
         					<li>Da Silva Marco</li>
         					<li>Kevin</li>
         					<li>Fribourg </li>
         					<li>Né le:05 mars 1995</li>
       			</ul>
      </div>

      <div id="Ecoles">
          <i><h3>Mes Ecoles </h3></i>
          <ul id="list">
              <li >Ecole primaire: Ecole du Jura (5 ans)</li>
              <li >Ecole secondaire: Cycle d'orientation (3 ans)</li>
              <li>Suite des Etudes: Ecole de Culture Générale Fribourg (3 ans) </li>
              <li>Suite des Etudes: Maturité Spécialisée Santé (1 an)</li>
              <li>Suite des Etudes: Haute Ecole de Sante Lausanne ( 1 an)</li>
              <li>Suite des Etudes: Année Futur-Ingénieur (1 an)</li>
              <li>Suite des Etudes: Haute Ecole D'ingénierie et d'Architecte (en cours)</li>
          </ul>
      </div>
      <div id="competences">
            <i><h3>Mes Competences </h3></i>
              <ul id="list">
                <li> Java </li>
                <li> HTML </li>
                <li> CSS </li>
                <li> PHP </li>
                <li> MySQL</li>
                <li> Outils de gestion</li>
                <li> C</li>
                <li> Informatique générale</li>
              </ul>
      </div>
      <div id="loisirs">
          <i><h3>Mes Loisirs </h3></i>
            <ul id="list">
              <li> Informatique <li>
              <li> Jeux video <li>
              <li> Lecture <li>
              <li> Cinéma <li>
            </ul>

      </div>
   </body>

</html>

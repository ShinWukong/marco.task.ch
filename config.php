<?php
	session_start();
if(isset($_SESSION['tri'])){
	if($_SESSION['tri'] == 'triprio'){
	$db = new PDO('mysql:host=localhost;dbname=Tasks;charset=utf8mb4','root','root');
	$stmt = $db -> prepare("SELECT task1.*,
												task1.id,
												task1.description,
												task1.created_at,
												author.name as author_name,
												assignee.name as assignee_name,
												executer.name as executer_name
												FROM task1
												INNER JOIN user author ON task1.created_by = author.id
												LEFT JOIN user assignee ON task1.assigned_to = assignee.id
												LEFT JOIN user executer ON task1.done_by = executer.id ORDER BY priority DESC");
	$stmt ->execute();
	$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

	} elseif ($_SESSION['tri'] == 'triDate') {
		$db = new PDO('mysql:host=localhost;dbname=Tasks;charset=utf8mb4','root','root');
		$stmt = $db -> prepare("SELECT task1.*,
													task1.id,
													task1.description,
													task1.created_at,
													author.name as author_name,
													assignee.name as assignee_name,
													executer.name as executer_name
													FROM task1
													INNER JOIN user author ON task1.created_by = author.id
													LEFT JOIN user assignee ON task1.assigned_to = assignee.id
													LEFT JOIN user executer ON task1.done_by = executer.id ORDER BY due_at DESC");
		$stmt ->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
	} elseif ($_SESSION['tri'] == 'triAuthor') {
		$db = new PDO('mysql:host=localhost;dbname=Tasks;charset=utf8mb4','root','root');
		$stmt = $db -> prepare("SELECT task1.*,
													task1.id,
													task1.description,
													task1.created_at,
													author.name as author_name,
													assignee.name as assignee_name,
													executer.name as executer_name
													FROM task1
													INNER JOIN user author ON task1.created_by = author.id
													LEFT JOIN user assignee ON task1.assigned_to = assignee.id
													LEFT JOIN user executer ON task1.done_by = executer.id ORDER BY created_by DESC");
		$stmt ->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
	} elseif ($_SESSION['tri'] == 'triOpen') {
		$db = new PDO('mysql:host=localhost;dbname=Tasks;charset=utf8mb4','root','root');
		$stmt = $db -> prepare("SELECT task1.*,
													task1.id,
													task1.description,
													task1.created_at,
													author.name as author_name,
													assignee.name as assignee_name,
													executer.name as executer_name
													FROM task1
													INNER JOIN user author ON task1.created_by = author.id
													LEFT JOIN user assignee ON task1.assigned_to = assignee.id
													LEFT JOIN user executer ON task1.done_by = executer.id WHERE status ='open' ");
		$stmt ->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
	} elseif ($_SESSION['tri'] == 'triClose') {
		$db = new PDO('mysql:host=localhost;dbname=Tasks;charset=utf8mb4','root','root');
		$stmt = $db -> prepare("SELECT task1.*,
													task1.id,
													task1.description,
													task1.created_at,
													author.name as author_name,
													assignee.name as assignee_name,
													executer.name as executer_name
													FROM task1
													INNER JOIN user author ON task1.created_by = author.id
													LEFT JOIN user assignee ON task1.assigned_to = assignee.id
													LEFT JOIN user executer ON task1.done_by = executer.id WHERE status ='close' ");
		$stmt ->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
	} elseif ($_SESSION['tri'] == 'triId') {
		$var = $_SESSION['userid'];
		$db = new PDO('mysql:host=localhost;dbname=Tasks;charset=utf8mb4','root','root');
		$stmt = $db -> prepare("SELECT task1.*,
													task1.id,
													task1.description,
													task1.created_at,
													author.name as author_name,
													assignee.name as assignee_name,
													executer.name as executer_name
													FROM task1
													INNER JOIN user author ON task1.created_by = author.id
													LEFT JOIN user assignee ON task1.assigned_to = assignee.id
													LEFT JOIN user executer ON task1.done_by = executer.id WHERE created_by = '$var' ");
		$stmt ->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
} else {
	$db = new PDO('mysql:host=localhost;dbname=Tasks;charset=utf8mb4','root','root');
		$stmt = $db->query("SELECT task1.*,
													task1.id,
													task1.description,
													task1.created_at,
													author.name as author_name,
													assignee.name as assignee_name,
													executer.name as executer_name
													FROM task1
													INNER JOIN user author ON task1.created_by = author.id
													LEFT JOIN user assignee ON task1.assigned_to = assignee.id
													LEFT JOIN user executer ON task1.done_by = executer.id");
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

?>

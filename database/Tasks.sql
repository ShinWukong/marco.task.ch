-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `task1`;
CREATE TABLE `task1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `due_at` date NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '1',
  `status` enum('open','close') NOT NULL DEFAULT 'open',
  `done_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `task1` (`id`, `description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `priority`, `status`, `done_by`) VALUES
(1,	'task 1',	'2017-06-25',	1,	'2017-06-25',	1,	3,	'open',	NULL),
(2,	'task 3',	'2017-06-25',	1,	'2017-01-24',	2,	2,	'open',	NULL),
(3,	'task 4',	'2017-06-25',	2,	'2017-01-25',	1,	4,	'open',	NULL),
(4,	'Text here',	'2017-06-25',	2,	'2017-06-29',	2,	2,	'open',	NULL),
(5,	'task closed',	'2017-06-26',	1,	'2017-06-26',	2,	3,	'close',	NULL);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1,	'patate',	'patate@patate.com',	'patate'),
(2,	'root',	'root@root.com',	'root');

-- 2017-06-26 08:24:43

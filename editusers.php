<?php
	require_once('config.php');
?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<title> Task Manager </title>
		<link rel="stylesheet" type="text/css" href="css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
</head>

<body>
				<?php require_once 'header.php';?>
  <div id="login">
            <h1 style="text-align:center"> Editer un utilisateur </h1>
            <?php
					$query = $db -> prepare('SELECT * FROM user WHERE id = ?');
	        $query -> execute(array($_GET['id']));
	        $data = $query -> fetch();
					?>
    <form  class="form" action="edituser-action.php" method="post">
      <input name="id" type="hidden" value="<?php echo $_GET['id']; ?>"/>

    <div class="top-row">

      <div class="field-wrap">
        <label>
          Nom
        </label>
        <input type="text"name="name" value="<?php echo $data['name']; ?>"/>
      </div>
    </div>

    <div class="field-wrap">
      <label>
      Adresse Email
      </label>
      <input type="email" name="email" value="<?php echo $data['email']; ?>"/>
    </div>

    <div class="field-wrap">
      <label>
      Mot de passe
      </label>
      <input type="password" name="password" value="<?php echo $data['password']; ?>"/>
    </div>

      <button type="submit" class="button button-block"/>Editer</button>
			<input class="button button-block" type="reset" value="Del">
    </form>
  </div>

	<span class="form-send">
	<a href="users.php" class="form-send-button"><button type="button" class="footer-button"><div class="footer-button-label">Back</div></button></a>
	  <?php require_once 'footer.php';?>
	</span>
	<script src="bower_components/jquery/dist/jquery.js"></script>
	<script src="bower_components/what-input/dist/what-input.js"></script>
	<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
	<script src="js/app.js"></script>
</body>
</html>

<header class="expanded row">
  <div class="title-bar">
    <div class="title-bar-left">
      <button class="menu-icon" type="button" data-open="offCanvasTop"></button>
      <span class="title-bar-title"><a href= "mainpage.php">MyTask</a></span>
      <ul class="dropdown menu" data-dropdown-menu>
        <li>
          <a href="#">Trier</a>
          <ul class="menu vertical">
            <li><a href="TaskByDueDate.php">Date due</a></li>
            <li><a href="TaskByAuthor.php">Auteur</a></li>
            <li><a href="TaskbyPriority.php">Priorité</a></li>
          </ul>
        </li>
        <li>
          <a href="#">Filtrer</a>
          <ul class="menu vertical">
            <li><a href="TaskByStatus.php">Terminées</a></li>
            <li><a href="TaskById.php">Mes tasks</a></li>
            <li><a href="TasksbyOpen.php">En cours</a></li>
          </ul>
        </li>
        <li>
          <li><a href="TaskbyPriority.php">Reset</a></li>
        </li>

      </ul>
    </div>
    <div class="title-bar-right">
      <?php
      $query =	$db -> prepare('SELECT * FROM user WHERE id = ?');
      $query -> execute(array($_SESSION['userid']));
      $data2 = $query -> fetch();
      ?>
      <ul class="dropdown menu" data-dropdown-menu>
        <li>
          <img src="assets/img/2B.jpg" alt="Photo de 2B" title="Photo"></img>
          <ul class="menu">
            <li><a href="edituser.php?id=<?php echo $_SESSION['userid']; ?>"><?php echo $data2['name']; ?></a></li>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <nav class="off-canvas position-left" id="offCanvasTop" data-off-canvas>
    <ul>
      <li><a href="users.php">liste utilisateurs</a></li>
      <li><a href="Adduser.php">ajouter un utilisateur</a></li>
      <li><a href="assets/PDF/manueltechnique.pdf"> Manuel Technique</a></li>
      <li><a href="assets/PDF/manueluser.pdf"> Manuel Utilisateur</a></li>
      <li><a href="assets/PDF/about.pdf"> About </a></li>
      <li><a href="about.php"> Auteur du site </a></li>
    </ul>
  </nav>
</header>

<?php
require_once('config.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title> Task Manager </title>
  <link rel="stylesheet" type="text/css" href="css/app.css" />
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
</head>
<body>
  <?php require_once 'headertitle.php';?>

  <div class="form">

    <div class="tab-content">
      <div id="signup">



        <div id="login">
          <h1>Connexion</h1>
          <form action="logme.php" method="post">


            <div class="field-wrap">
              <label>
                Adresse Email<span class="req">*</span>
              </label>
              <input type="email"required autocomplete="off" name="email"/>
            </div>

            <div class="field-wrap">
              <label>
                Mot de passe<span class="req">*</span>
              </label>
              <input type="password" required autocomplete="off" name="password"/>
            </div>

            <input type="submit" value="Login" class="button"></input>
          </form>
          <h2>Inscription</h2>
          <a href="SignIn.php" class="button">Nouveau compte</a>
        </div>
      </div>

    </div>
  </body>
  <script src="bower_components/jquery/dist/jquery.js"></script>
  <script src="bower_components/what-input/dist/what-input.js"></script>
  <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
  <script src="js/app.js"></script>
  </html>

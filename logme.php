<?php require_once('config.php');

$stmt = $db->prepare("SELECT id FROM user WHERE email=? AND password=?");
$stmt->execute( array($_REQUEST['email'], $_REQUEST['password']));

$data= $stmt->fetch();

if($data){
  $_SESSION['userid'] = $data['id'];
  header('location:mainpage.php');
}
else{
  unset($_SESSION['userid']);
  header('Location:index.php');
}

?>

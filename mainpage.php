
<?php
require_once('config.php');
require_once('security.php');
?>

<!doctype html>

<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Foundation for Sites</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <?php require_once 'headermain.php';?>
  <h1 class="page-title">Liste des tâches</h1>
  <section class="row-tasklist">
  </section>
  <div class="tasklist">
    <main>
      <ul class="tasklist-tab">
        <li class="tasklist-head">
          <span class="tasklist-item-id">Id</span>
          <span class="tasklist-item-description">Description</span>
          <span class="show-for-large tasklist-item-createdate">Date</span>
          <span class="tasklist-item-date">Due</span>
          <span class="show-for-large tasklist-item-authorname">Author</span>
          <span class="show-for-large tasklist-item-assigneename">Assigned to</span>
          <span class="tasklist-item-priority">Priority</span>
          <span class="tasklist-item-status">status</span>
        </li>

        <?php foreach ($data as $row):?>
          <?php if( $row['status'] == "done"): ?>
            <li class="isdone">
            <?php else : ?>
              <li class="tasklist-body">
          <?php endif; ?>
              <span class="tasklist-item-id"><?php echo $row['id']?></span>
              <span class="tasklist-item-description"><?php echo $row['description']?></span>
              <span class="show-for-large tasklist-item-createdate"><?php echo $row['created_at']?></span>
              <span class="tasklist-item-date"><?php echo $row['due_at']?></span>
              <span class="show-for-large tasklist-item-authorname"><?php echo $row['author_name']?></span>
              <span class="show-for-large tasklist-item-assigneename"><?php echo $row['assignee_name']?></span>
              <span class="tasklist-item-priority"><?php echo $row['priority']?></span>
              <span class="tasklist-item-status"><?php echo $row['status']?></span>
              <span class="tasklist-item-done"><a href="#" data-done="<?php echo $row['id']?>"><button type="button" class="tasklist-item-finish">✔</button></a></span>
              <span class="tasklist-item-update"><a href="edit.php?id=<?php echo $row['id']; ?>"><button type="button" class="tasklist-item-refresh">↺</button></a></span>
              <span class="tasklist-item-delete"><a href="#" data-delete="<?php echo $row['id']?>"><button type="button" class="tasklist-item-erase">✘</button></a></span>
            </li>
        <?php endforeach;?>
      </ul>
    </main>
  </div>
  <footer class="row-footer">

    <a href="newtask.php"><button type="button" class="footer-button"><div class="footer-button-label">New</div></button></a>
    <?php require_once 'footer.php';?>
  </footer>



    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
  </html>

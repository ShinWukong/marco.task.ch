	<?php
		require_once('config.php');
	  require_once('security.php');
	?>
	<!DOCTYPE html>
	<html>
	<head>
			<meta charset="utf-8">
			<title> Task Manager </title>
			<link rel="stylesheet" type="text/css" href="css/app.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
	</head>

	<body>
		<?php require_once 'header.php'; ?>
		<h1 style="text-align:center"> Nouvelle tâche</h1>
		<form class="form" method="post" action="insert.php">
			<span class="form">Description:</span>
		 <span><textarea class="form" name="description" value="description">Text here</textarea></span>

			 <span class="form">Date à rendre:</span>
		  <input class="form" type="date" name="due_at" value="due_at">

			<span class="form">Priorité:</span>
				<span><select class="form" name="priority" value="<?php echo $data['priority']; ?>">
				              <?php for($i = 1; $i <= 4; $i++): ?>
				                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				              <?php endfor; ?>
				</select></span>
				<span class="form">Assigné à</span>
				<span><select class="form" name="assigned_to">
						<?php
						 		$stmt = $db -> query('SELECT * FROM user');
							  while($data= $stmt-> fetch()):
						?>
									<option value="<?php echo $data['id'];?>">
											<?php echo $data['name'];?></option>
						<?php endwhile;?>
							</select></span>

			<span class="form-send">
			<input class="button" type="submit" value="Envoyer">
			<input class="button" type="reset" value="Del"></span>
			<a href="mainpage.php" class="form-send-button"><button type="button" class="footer-button"><div class="footer-button-label">Back</div></button></a>
			  <?php require_once 'footer.php';?>
		</form>
	</body>

	<script src="bower_components/jquery/dist/jquery.js"></script>
	<script src="bower_components/what-input/dist/what-input.js"></script>
	<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
	<script src="js/app.js"></script>
	</html>

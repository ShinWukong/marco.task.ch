<?php
require_once('config.php');
require_once('security.php');
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title> Task Manager </title>
  <link rel="stylesheet" type="text/css" href="css/app.css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
</head>
<body>
  <div class="off-canvas-wrapper">
    <?php require_once('header.php');	?>

    <main class="container off-canvas-content" data-off-canvas-content>
      <div class="row">
        <h1 class="page-title">Liste des utilisateurs</h1>
        <ul class="tasklist">
          <li class="tasklist-header">
            <span class="tasklist-item-id">ID</span>
            <span class="tasklist-item-authorname"> Utilisateur </span>
            <span class="tasklist-item-description">Email</span>
            <span class="tasklist-item-actions">Actions </span>
          </li>
          <?php
          $query = $db -> query('SELECT * FROM user');
          while($data = $query -> fetch()):
            ?>
            <li class="tasklist-item">
              <span class="tasklist-item-id">
                <?php echo $data['id']; ?>
              </span>
              <span class="tasklist-item-authorname">
                <?php echo $data['name']; ?>
              </span>
              <span class="tasklist-item-description">
                <?php echo $data['email']; ?>
              </span>
              <span class="tasklist-item-actions">
                <a href="editusers.php?id=<?php echo $data['id'];?>"><button type="button" class="tasklist-item-refresh">↺</button></a>
              </span>
              <span class="tasklist-item-actions">
                <a href="#" data-deleteuser="<?php echo $data['id'];?>"><button type="button" class="tasklist-item-erase">✘</button></a></span>
              </span>
            </li>
          <?php endwhile; ?>
        </ul>
      </div>
    </main>
  </div>
  <span class="form-send">
    <a href="mainpage.php" class="form-send-button"><button type="button" class="footer-button"><div class="footer-button-label">Back</div></button></a>
    <?php require_once 'footer.php';?>
  </span>
  <script src="bower_components/jquery/dist/jquery.js"></script>
  <script src="bower_components/what-input/dist/what-input.js"></script>
  <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
